class TestClass (var value2:String){
  private[this] var value :String = value2
  var member = ""

  println("constructor "+value2)
  def combine(t : TestClass) = {
    value+=value2+t.getValue
    println("value="+value)
  }

  def getValue: String= value
}

object TestClass {
  def staticMethod() = {
    println("i m static")
  }
  def apply(value2 : String) ={
    println("apply function "+value2)

  }
}

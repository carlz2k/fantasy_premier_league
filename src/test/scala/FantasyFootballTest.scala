import com.typesafe.config.ConfigFactory
import org.apache.spark.SparkConf
import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.sql.SparkSession._
import org.apache.spark.sql.functions.explode
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.joda.time.DateTime.now

object FantasyFootballTest extends App {

  //https://github.com/tgreyjs/fpl-api-node/blob/master/src/data.service.ts
  //https://fantasy.premierleague.com/drf/leagues-classic-standings/12
  //https://fantasy.premierleague.com/drf/entry/3144479/event/2/picks
  //https://fantasy.premierleague.com/drf/event/1/live
  private val FANTASY_FOOTBALL_JSON_FILE_PATH =
  "src/test/resources/fantasy_football/fantasy-football.json"
  private val FANTASY_FOOTBALL_JSON_FILE_PATH =
    "src/test/resources/fantasy_football/picks.json"

  private val PLAYER_DATA_SCHEMA_NAME = "player"

  private val SELECT_PLAYER_PARAMETERS_SQL_SEGMENT =
    s"""select $PLAYER_DATA_SCHEMA_NAME.web_name, $PLAYER_DATA_SCHEMA_NAME.element_type,
      $PLAYER_DATA_SCHEMA_NAME.value_form,
      $PLAYER_DATA_SCHEMA_NAME.value_season,
      $PLAYER_DATA_SCHEMA_NAME.team, $PLAYER_DATA_SCHEMA_NAME.in_dreamteam,
      $PLAYER_DATA_SCHEMA_NAME.dreamteam_count, $PLAYER_DATA_SCHEMA_NAME.form,
      $PLAYER_DATA_SCHEMA_NAME.chance_of_playing_this_round,
      $PLAYER_DATA_SCHEMA_NAME.chance_of_playing_next_round,
      $PLAYER_DATA_SCHEMA_NAME.cost_change_start,
      $PLAYER_DATA_SCHEMA_NAME.cost_change_event,
      $PLAYER_DATA_SCHEMA_NAME.selected_by_percent,
      $PLAYER_DATA_SCHEMA_NAME.total_points,
      $PLAYER_DATA_SCHEMA_NAME.points_per_game,
      $PLAYER_DATA_SCHEMA_NAME.ep_this,
      $PLAYER_DATA_SCHEMA_NAME.ep_next,
      $PLAYER_DATA_SCHEMA_NAME.minutes,
      $PLAYER_DATA_SCHEMA_NAME.goals_scored,
      $PLAYER_DATA_SCHEMA_NAME.assists,
      $PLAYER_DATA_SCHEMA_NAME.clean_sheets,
      $PLAYER_DATA_SCHEMA_NAME.goals_conceded,
      $PLAYER_DATA_SCHEMA_NAME.yellow_cards,
      $PLAYER_DATA_SCHEMA_NAME.red_cards,
      $PLAYER_DATA_SCHEMA_NAME.bonus,
      $PLAYER_DATA_SCHEMA_NAME.bps,
      $PLAYER_DATA_SCHEMA_NAME.influence,
      $PLAYER_DATA_SCHEMA_NAME.creativity,
      $PLAYER_DATA_SCHEMA_NAME.threat,
      $PLAYER_DATA_SCHEMA_NAME.ict_index,
      $PLAYER_DATA_SCHEMA_NAME.transfers_out,
      $PLAYER_DATA_SCHEMA_NAME.transfers_in """

  private val SELECT_ALL_PLAYERS_DATA_SQL = SELECT_PLAYER_PARAMETERS_SQL_SEGMENT +
    s"from $PLAYER_DATA_SCHEMA_NAME"


  private def initSparkSession(): SparkSession = {
    System.setProperty("hadoop.home.dir", "/Users/carl.zhang/hadoop-3.0.0")

    val config = ConfigFactory.load()
    val conf = new SparkConf().setAppName(config.getString("spark.master"))
      .setMaster(config.getString("spark.master"))
      .set("spark.debug.maxToStringFields", config.getString("spark.max-to-string-fields"))

    builder()
      .config(conf)
      .getOrCreate()
  }

  private def loadPlayerData(sparkSession: SparkSession): Unit = {
    val jsonDf = sparkSession.read.json(FANTASY_FOOTBALL_JSON_FILE_PATH)

    jsonDf.select(explode(jsonDf("elements"))).toDF(PLAYER_DATA_SCHEMA_NAME)
      .createOrReplaceTempView(PLAYER_DATA_SCHEMA_NAME)
  }

  private def getPlayerDataFrame(sparkSession: SparkSession): DataFrame = {
    loadPlayerData(sparkSession)
    sparkSession.sql(SELECT_ALL_PLAYERS_DATA_SQL)
  }

  private def getPlayerDataFrameById(sparkSession: SparkSession, id: Int): Unit = {
    sparkSession.sql(s"$SELECT_PLAYER_PARAMETERS_SQL_SEGMENT from $PLAYER_DATA_SCHEMA_NAME where" +
      s" $PLAYER_DATA_SCHEMA_NAME.id = " + id)
      .show()
  }

  def run(): Unit = {

    val sparkSession = initSparkSession()

    val df = getPlayerDataFrame(sparkSession)

    println(df.schema)

    getPlayerDataFrameById(sparkSession, 331)

    val trainingDataTestDataSplit = 0.7

    val splits = df.randomSplit(Array(trainingDataTestDataSplit, 1 - trainingDataTestDataSplit),
      now().getMillis)

    val trainingData = splits(0)

    val testData = splits(1)

    val model = new LogisticRegressionWithLBFGS().setNumClasses(2).run(trainingData.rdd
      .map {
        row => {
          val inDrameTeam: Boolean = row.getAs("in_dreamteam")
          val name: String = row.getAs("web_name")
          LabeledPoint(1, Vectors.dense(2, 5))
        }
      })

    // Evaluate model on training examples and compute training error
    //    val labelAndPreds = testData.rdd
    //      .map {
    //        row =>
    //          //println("line2=" + line)
    //          val finishedLabel = isFinishedOnTime(row)
    //          val isLargeProjectParam = isLargeProject(getStoryPoint(row))
    //          val isLargeEstimateParam = isLargeEsimate(stringToDouble(row.getString(getIndex("? Original Estimate"))))
    //          LabeledPoint(finishedLabel, Vectors.dense(isLargeProjectParam, isLargeEstimateParam))
    //      }
    //      .map { point =>
    //        val prediction = model.predict(point.features)
    //        (point.label, prediction)
    //      }
    //
    //    val trainErr = labelAndPreds.filter(r => r._1 != r._2).count.toDouble / testData.count
    //
    //    println("error = " + trainErr)
    //
    //    val model2 = new LogisticRegressionWithLBFGS().setNumClasses(2).run(trainingData.rdd
    //      .filter {
    //        row =>
    //          !Strings.isNullOrEmpty(row.getString(getIndex("Assignee"))) && !row.getString(getIndex("Assignee")).equalsIgnoreCase("Unassigned")
    //      }.map {
    //      row => {
    //        println("line1=" + row)
    //        val finishedLabel = isFinishedOnTime(row)
    //        LabeledPoint(finishedLabel, Vectors.dense(row.getString(getIndex("Assignee")).hashCode))
    //      }
    //
    //    })
    //
    //    Evaluate model on training examples and compute training error
    //    val labelAndPreds2 = testData.rdd
    //      .filter {
    //        line =>
    //          !Strings.isNullOrEmpty(line.getString(getIndex("Assignee"))) && !line.getString(getIndex("Assignee")).equalsIgnoreCase("Unassigned")
    //      }
    //      .map {
    //        row =>
    //          //println("line2=" + line)
    //          val finishedLabel = isFinishedOnTime(row)
    //          LabeledPoint(finishedLabel, Vectors.dense(row.getString(getIndex("Assignee")).hashCode))
    //      }
    //      .map { point =>
    //        val prediction = model2.predict(point.features)
    //        (point.label, prediction)
    //      }
    //
    //    val trainErr2 = labelAndPreds2.filter(r => r._1 != r._2).count.toDouble / testData.count
    //
    //    println("error = " + trainErr2)

    //  private def isFinishedOnTime(data: Row): Int = {
    //    var finishedLabel = 1
    //    var remainingEstimate = 0
    //    if (!Strings.isNullOrEmpty(data.getString(getIndex("? Remaining Estimate")))) {
    //      remainingEstimate = data.getString(getIndex("? Remaining Estimate")).toInt
    //    }
    //    var originalEstimate = 0
    //
    //    if (!Strings.isNullOrEmpty(data.getString(getIndex("? Original Estimate")))) {
    //      originalEstimate = data.getString(getIndex("? Original Estimate")).toInt
    //    }
    //
    //    if (data.getString(getIndex("Status")) != "Done" && remainingEstimate == 0 && originalEstimate != 0) {
    //      finishedLabel = 0
    //    }
    //
    //    finishedLabel
    //  }
    //
    //  private def getStoryPoint(data: Row): Int = {
    //    var storyPoint = 0
    //    if (!Strings.isNullOrEmpty(data.getString(getIndex("Story Points")))) {
    //      storyPoint = data.getString(getIndex("Story Points")).toInt
    //    }
    //    storyPoint
    //  }

    //  private def isLargeProject(storyPoint: Double): Double = {
    //    if (storyPoint > 5) {
    //      1.0
    //    } else {
    //      0.0
    //    }
    //  }
    //
    //  private def isLargeEsimate(estimate: Double): Double = {
    //    if (estimate > 160000) {
    //      1.0
    //    } else {
    //      0.0
    //    }
    //  }
  }

  run()
}

name := "spark-machine-learning"

version := "1.0"

val sparkVersion = "2.3.0"
val scalaVer = "2.11.12"

scalaVersion := scalaVer


libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "com.typesafe" % "config" % "1.3.3",
  "org.scalatest" % "scalatest_2.11" % "3.0.5" % "test"
)

